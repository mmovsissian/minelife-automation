describe("Manage jobs", () => {

    it(`should have Candidate funnel displayed`, () => {


        cy.visit("/jobs/manage-jobs/search", { timeout: 120000 })
        cy.wait(2000)

        cy.get('input[placeholder="Search"]').should('be.visible')
        cy.get('ng-select[placeholder="Company"]').click()
        // cy.get('div[id="acbbea7f3b17-1"]').click()
        cy.get('div[class="ng-dropdown-panel-items scroll-host"] div[class=\'ng-option\']').eq(1).click()
        cy.wait(3000)
        //
        cy.get('div[class="filter__title"]').last().should('contain','Candidate funnel')
        cy.get('div[class="filter"]').last().click({force: true})
        cy.wait(2000)
        cy.get('button[class="ml-btn ml-btn-primary"]').should('be.visible').should('contain','Invite to job')

        cy.get('button[class="ml-btn ml-btn-primary"]')

    });
});

"npx percy exec -- cypress run --spec 'cypress/integration/hire_page_test.spec.js'"
