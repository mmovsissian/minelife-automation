describe("Investiors and Media - Photo Gallery", () => {

    beforeEach(() => {
        cy.viewport(1440, 990)
        cy.login()
        cy.visit("/dashboard/2108/investors-and-media/photo-gallery", {timeout: 20000})
    })

    it(`should navigate back to "Investors and Media" page`, function () {
        cy.get('.photo-gallery-menu .menu-back-link').should(
            "contain.text",
            "Investors and Media").click()
        cy.url().should('eq', 'https://minelife.org/dashboard/2108/investors-and-media')
    })

    it(`should have "Photo Gallery" section and Editing`, function () {
        cy.get('.photo-gallery-menu .edit-section-name').should(
            "contain.text",
            "Photo Gallery")
        cy.get('.photo-gallery-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.photo-gallery-menu .is-edit').should("be.visible")
        cy.get('.photo-gallery-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.photo-gallery-menu .is-edit').should("not.be.visible")
    })


    it(`should have "Manage Gallery", sections and albums`, function () {
        cy.get('.header-wrapper .header-title').should("be.visible").should(
            "contain.text",
            "Manage Gallery")
        cy.get('.header-wrapper .header-sub-title').should("be.visible").should(
            "contain.text",
            "Choose a section and add albums")

        cy.get('.header-wrapper .header-tab-title').then(($el) => {
            let number_of_sections = $el.length
            cy.log("number of sections is: " + number_of_sections)
            for (let i = 0; i < number_of_sections; i++) {
                cy.get('.header-wrapper .header-tab-title').eq(i).click().then(($el) => {
                    cy.log("Cheching album section: " + $el.text())
                    cy.get('.photo-gallery-body .photo-gallery-add-button').should("be.visible")
                    cy.get('.photo-gallery-body .photo-gallery-add-label').should("be.visible").should(
                        "contain.text",
                        $el.text())

                })
            }
        })
    })

    it(`should be able to Add/Remove new albums`, function () {
        cy.get('.header-wrapper .header-tab-title').eq(0).click()
        cy.wait(1000)
        cy.get('.photo-gallery-body .photo-gallery-add-button').should("be.visible").click()
        cy.wait(1000)
        cy.visit("/dashboard/2108/investors-and-media/photo-gallery", {timeout: 20000})

        cy.request('GET', "https://minelife.org/api/dashboard/Company/2108/InvestorsAndMedia/Photos").then(
            (response) => {
                cy.log(response.body["investorsAndMediaAlbums"].length)
                cy.scrollTo('bottom')

                cy.get('input[placeholder="Album"]').eq(response.body["investorsAndMediaAlbums"].length - 1).and('have.value', '').parent('div').parent('div').parent('div').within(() => {

                    cy.get('.album-images-remove-album').should("be.visible").click()
                });
            })
        cy.get('.btn-primary').click()
        cy.wait(2000)
        cy.get('input[placeholder="Album"]').last().should('not.have.value', 'Album')
    })
})

describe("Investiors and Media - Documents", () => {

    beforeEach(() => {
        cy.viewport(1440, 990)
        cy.login()
        cy.visit("/dashboard/2108/investors-and-media/documents", {timeout: 20000})
    })

    it(`should navigate back to "Investors and Media" page`, function () {
        cy.get('.documents-menu .menu-back-link').should(
            "contain.text",
            "Investors and Media").click()
        cy.url().should('eq', 'https://minelife.org/dashboard/2108/investors-and-media')
    })

    it(`should have "Documents" section and Editing`, function () {
        cy.get('.documents-menu .edit-section-name').should(
            "contain.text",
            "Documents")
        cy.get('.documents-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.documents-menu .is-edit').should("be.visible")
        cy.get('.documents-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.documents-menu .is-edit').should("not.be.visible")
    })

    it(`should have "Manage Documents", sections and folders`, function () {
        cy.wait(1000)
        cy.scrollTo('top')
        cy.get('.header-wrapper .header-title').should("be.visible").should(
            "contain.text",
            "Manage Documents")
        cy.get('.header-wrapper .header-sub-title').should("be.visible").should(
            "contain.text",
            "Choose folder and add documents")

        cy.get('.header-wrapper .header-tab-title').then(($el) => {
            let number_of_sections = $el.length
            cy.log("number of sections is: " + number_of_sections)
            for (let i = 0; i < number_of_sections; i++) {


                cy.get('.header-wrapper .header-tab-title').eq(i).click().then(($el) => {
                    cy.wait(1000)
                    cy.scrollTo('top')
                    cy.log("Cheching album section: " + $el.text())
                    cy.get('.documents-body .documents-add-button').should("be.visible")
                    cy.get('.documents-body .documents-add-label').should("be.visible").should(
                        "contain.text",
                        $el.text())
                })
            }
        })
    })

    it(`should be able to Add/Remove new folders`, function () {
        cy.get('.header-wrapper .header-tab-title').eq(0).click()
        cy.wait(1000)
        cy.scrollTo('top')
        cy.get('.documents-body .documents-add-button').should("be.visible").click()
        cy.wait(1000)

        cy.visit("/dashboard/2108/investors-and-media/documents", {timeout: 20000})

        cy.request('GET', "https://minelife.org/api/dashboard/Company/2108/InvestorsAndMedia/Documents").then(
            (response) => {
                cy.log(response.body["investorsAndMediaAlbums"].length)
                cy.scrollTo('bottom')

                cy.get('input[placeholder="Album"]').eq(response.body["investorsAndMediaAlbums"].length - 1).and('have.value', '').parent('div').parent('div').parent('div').within(() => {

                    cy.get('.sub-folder-files-remove-sub-folder').should("be.visible").click().click()
                });
            })
        cy.get('.btn-primary').click()
        cy.wait(2000)
        cy.get('input[placeholder="Album"]').last().should('not.have.value', 'Album')
    })
})

describe("Investiors and Media - Capital & Share Structure", () => {

    beforeEach(() => {
        cy.viewport(1440, 990)
        cy.login()
        cy.visit("/dashboard/2108/investors-and-media/capital-and-share-structure", {timeout: 20000})
    })

    it(`should navigate back to "Investors and Media" page`, function () {
        cy.get('.capital-and-share-structure-menu .menu-back-link').should(
            "contain.text",
            "Investors and Media").click()
        cy.url().should('eq', 'https://minelife.org/dashboard/2108/investors-and-media')
    })

    it(`should have "Capital & Share Structure" section and Editing`, function () {
        cy.get('.capital-and-share-structure-menu .edit-section-name').should(
            "contain.text",
            "Capital & Share Structure")
        cy.get('.capital-and-share-structure-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.capital-and-share-structure-menu .is-edit').should("be.visible")
        cy.get('.capital-and-share-structure-menu .ml-icon-pencil').should("be.visible").click()
        cy.get('.capital-and-share-structure-menu .is-edit').should("not.be.visible")
    })


    it(`should be able to Add/Remove Text Areas`, function () {

        cy.get('    .capital-and-share-structure-header .ml-button').should("be.visible").should(
            "contain.text",
            "Add Text Area").click()

        cy.wait(1000)
        cy.scrollTo('bottom')

        cy.get('div[class="section-actions-header__title"] span').last().should('have.value', '').parent('div').parent('div').within(() => {

            cy.get('.ml-icon-bin').should("be.visible").click()
        });
        cy.get('.btn-primary').click()
        cy.wait(1000)
        cy.scrollTo('bottom')
    })
})
