describe("Hire ExpertsPage", () => {

    // user details:
    let user = "Clive Hastings"
    let country = "Tanzania"
    let info = "Australian Contract Project Exploration Geologist based in Africa"


    it(`should have search by name`, function () {
        cy.visit("/experts/hire-experts", {timeout: 20000})
        cy.getElement('input[placeholder="Search by Name"]').should('be.visible').type(user)
        cy.percySnapshot('/experts/hire-experts Page', {widths: [1280]});
        cy.wait(1000)
        cy.getElement('span[class="candidates_found_left_number"]').contains("1")
        cy.wait(5000)

        cy.percySnapshot(`should have search by name`, {widths: [1280]});
    });

    it(`user details should be displayed`, () => {
        cy.get('p[class="info__name"]').contains(user)
        cy.getElement('span[class="info__state"]').contains(info)
    });


    it(`user Profile Dialog be displayed`, () => {
        cy.wait(2000)
        cy.get('button[class="ml-btn ml-btn-secondary hover"]').click()
        // cy.wait(2000)
        cy.get('span[class="user-info__full-name"]').contains(user)
        cy.get('span[class="user-info__location"]').contains(country)

        cy.percySnapshot(`user Profile Dialog be displayed`, {widths: [1280]});
        cy.get('div[class="remove-icon"]').click()
        cy.get('input[placeholder="Search by Name"]').should('be.visible')
    });

    it(`should open Invite to Jobs dialog`, () => {
        cy.login()
        cy.visit("/experts/hire-experts", {timeout: 20000})
        cy.wait(5000)
        cy.get('button[class="ml-btn ml-btn-secondary hover"]').first().click()
        cy.wait(5000)
        cy.get('div[class="user-info"] button[class="ml-btn ml-btn-primary"]').eq(1).click()
        cy.wait(5000)
        cy.percySnapshot(`should open Invite to Jobs dialog`, {widths: [1280]});
    });

    it(`should open /manage-jobs/search from Manage jobs button`, () => {
        cy.login()
        cy.visit("/experts/hire-experts", {timeout: 20000})

        // cy.get('button[class="ml-btn ml-btn-secondary hover"]').click()
        cy.wait(8000)

        cy.get('div[class="candidates_found_right"] button[class="ml-btn ml-btn-primary"]').click()
        cy.wait(2000)

        cy.url().should('eq', 'https://minelife.org/jobs/manage-jobs/search')
    });

    it(`should open /jobs from Post a Job button`, () => {
        cy.login()
        cy.visit("/experts/hire-experts", {timeout: 20000})

        cy.wait(5000)

        cy.get('div[class="candidates_found_right"] button[class="ml-btn ml-btn-secondary"]').click()
        cy.wait(2000)

        cy.url().should('eq', 'https://minelife.org/jobs/')
    });

    it(`Adding/Clearing filters in Add Filter dialog`, () => {
        cy.login()
        cy.visit("/experts/hire-experts", {timeout: 20000})
        cy.wait(5000)
        cy.get('div[class="add-filter"]').click()
        cy.wait(1000)
        cy.percySnapshot(`Adding/Clearing filters in Add Filter dialog`, {widths: [1280]});
        cy.get('span[class="candidates_found_left_text"]').contains("Candidates found")

        cy.get('input[placeholder="Search locations"]').type("mexico")
        cy.wait(2000)
        cy.get('div[class="ml-form__group suggestion-filter__item"]').click()
        cy.wait(2000)
        cy.get('button[class="ml-btn ml-btn-primary full-width"').click()

        cy.get('div[class="filter-left-block"] div[class="ml-form__group"] div').eq(1).click()

        cy.get('div[class="filter-block"]').eq(0).within(() => {
            cy.get('div').eq(0).click()
        });
        cy.wait(1000)

        cy.get('div[class="filter-block"]').eq(4).within(() => {
            cy.get('div').eq(0).click()
        });
        cy.wait(1000)
        cy.get('div[class="filter-block filter-block_full"] div[class="ml-form__group filter-search__checkbox"]').first().click()

        cy.get('div[class="filter-footer ml-mt-15"] button[class="ml-btn ml-btn-secondary"]').click()
        cy.get('div[class="clear-filter ml-link active"]').contains(" Clear (4) ").click()
        cy.get('div[class="clear-filter ml-link"]').contains(" Clear (0) ")
    });


    it(`Add filters on Hire page `, () => {

        cy.visit("/experts/hire-experts", {timeout: 20000})
        cy.wait(5000)
        cy.get('div[class="filter-container"]').eq(0).within(() => {
            cy.get('div').eq(0).click()
        });

        cy.get('input[placeholder="Deposit types"]').type("vms")
        cy.wait(2000)
        cy.get('div[class="ml-form__group suggestion-filter__item"]').click()
        cy.wait(2000)
        cy.get('button[class="ml-btn ml-btn-primary full-width"').click()
        cy.wait(2000)

        cy.get('div[class="filter-container"]').eq(2).within(() => {
            cy.get('div').eq(0).click()
        });


        cy.wait(2000)
        cy.get('div[class="custom-filter-heading"]').first().click()

        cy.get('div[class="tab_content"] div[class="custom-filter__checkbox"]').eq(0).within(() => {
            cy.get('div').eq(0).click()
        });
        cy.wait(1000)
        cy.get('div[class="clear-filter ml-link active"]').contains(" Clear (4) ").click()
        cy.get('div[class="clear-filter ml-link"]').contains(" Clear (0) ")
    });

    it(`Navigation through Involvement Type filters `, () => {
        cy.visit("/experts/hire-experts", {timeout: 20000})
        cy.wait(5000)

        cy.get('ul[class="involvement-type-filter__menu-wrapper"] li').eq(1).click()
        cy.wait(1000)
        cy.get('span[class="filter-wrapper__heading"]').first().contains("Drilling Experience")
        cy.get('ul[class="involvement-type-filter__menu-wrapper"] li').eq(2).click()
        cy.wait(1000)

        cy.get('span[class="filter-wrapper__heading"]').first().contains("Blasters ")
    });

});

