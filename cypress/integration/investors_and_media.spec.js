describe("Investiors and Media - Header", () => {

    // user details:
    let invmedia_header = ".Investors-and-media-header__"

    beforeEach(() => {
        cy.viewport(1440, 990)

    })

    before(() => {
        cy.visit("/dashboard/2108/investors-and-media", {timeout: 20000})

    })

    it(`should have Investiors and Media page Title`, function () {
        cy.getElement('.menu-title').first().should(
            "have.text",
            "INVESTORS-AND-MEDIA"
        )
    });

    it(`should have Header/Body/Style tabs`, function () {

        cy.getElement('.tabs-panel .ng-star-inserted').eq(0).should(
            "contain.text",
            "Header"
        )
        cy.getElement('.tabs-panel .ng-star-inserted').eq(1).should(
            "contain.text",
            "Body"
        )
        cy.getElement('.tabs-panel .ng-star-inserted').eq(2).should(
            "contain.text",
            "Style"
        )
    });

    it(`should have "Height Setting" Title`, function () {
        cy.getElement(`${invmedia_header}height-settings--title`).first().should(
            "contain.text",
            "Height setting"
        )

    });

    it(`should have "Height Setting" S/M/L`, function () {
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(0).should(
            "contain.text",
            "S"
        )
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(1).should(
            "contain.text",
            "M"
        )
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(2).should(
            "contain.text",
            "L"
        )
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(3).should(
            "contain.text",
            "Image Height"
        )
    });

    it(`should have "Height Setting" Image Height Slider and Input`, function () {
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(3).should(
            "contain.text",
            "Image Height"
        )

        cy.getElement(`${invmedia_header}height-settings--picker`).first().within(() => {
            cy.get('input[formcontrolname="rangeSliderField"]').should('be.visible')
        });

        cy.getElement(`${invmedia_header}height-settings--picker`).first().within(() => {
            cy.get('input[formcontrolname="rangeInputField"]').should('be.visible')
        });
    });

    it(`should have height settings changing values`, function () {
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(0).click()
        cy.getElement(`${invmedia_header}height-settings--picker`).first().within(() => {
            cy.get('input[formcontrolname="rangeInputField"]').should('have.value', "340px")
        });
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(1).click()
        cy.getElement(`${invmedia_header}height-settings--picker`).first().within(() => {
            cy.get('input[formcontrolname="rangeInputField"]').should('have.value', "570px")
        });

        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(2).click()
        cy.getElement(`${invmedia_header}height-settings--picker`).first().within(() => {
            cy.get('input[formcontrolname="rangeInputField"]').should('have.value', "800px")
        });
        cy.getElement(`${invmedia_header}height-settings--picker .ng-star-inserted`).eq(0).click()
    });

    it(`should have "Apply Text to Photo"`, function () {
        cy.getElement(`${invmedia_header}style-settings--apply-text .ml-form__label`).should('be.visible').should(
            "contain.text",
            "Apply Text to Photo")

        cy.getElement(`${invmedia_header}style-settings--background  .ng-star-inserted`).should('be.visible').should(
            "contain.text",
            "Background Color and Transparency")

        // cheching text settings not visible when "apply to text" checkbox not selected

        cy.getElement(`${invmedia_header}style-settings--apply-text .ml-form__label`).click()
        cy.getElement(`${invmedia_header}style-settings--background  .ng-star-inserted`).should('not.be.visible')
        cy.getElement(`${invmedia_header}style-settings--apply-text .ml-form__label`).click()
        cy.getElement(`${invmedia_header}style-settings--background  .ng-star-inserted`).should('be.visible')
    });

    it(`should have "Title"`, function () {
        cy.getElement(`${invmedia_header}style-settings--title .title`).should('be.visible').should(
            "contain.text",
            "Title")

        //checking title text area visible
        cy.getElement(`${invmedia_header}style-settings--title .ml-form__input`).should('be.visible')

        // getting title text
        cy.getElement(`${invmedia_header}style-settings--title .ml-form__input`).invoke('val').then(titletext =>
            // check text is applied on the page
            cy.getElement(`.top-section__header--text-field .header-title`).should('be.visible').should(
                "contain.text",
                titletext))
    });

    it(`should have "SubTitle"`, function () {
        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')

        cy.getElement(`${invmedia_header}style-settings--subtitle .title`).should('be.visible').should(
            "contain.text",
            "Subtitle")
        //checking title text area visible
        cy.getElement(`${invmedia_header}style-settings--subtitle .ml-form__input`).should('be.visible')

        // getting title text
        cy.getElement(`${invmedia_header}style-settings--subtitle .ml-form__input`).invoke('val').then(titletext =>
            // check text is applied on the page
            cy.getElement(`.top-section__header--text-field .header-sub-title`).should('be.visible').should(
                "contain.text",
                titletext))
    });

    it(`should have "Text Size and Color" for Title`, function () {

        cy.getElement('.menu-scrollbar').first().scrollTo(0, 500)

        // Checking Title
        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__label`).eq(1).should('be.visible').should(
            "contain.text",
            "Text Size and Color")

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__color`).eq(1).should('be.visible').should(
            "contain.value",
            "#")

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__range`).eq(1).should('be.visible')

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__input`).eq(1).should('be.visible').should(
            "contain.value",
            "px")
    });

    it(`should have "Text Size and Color" for Subtitle`, function () {

        cy.getElement('.menu-scrollbar').first().scrollTo(0, 500)

        // Checking SubTitle
        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__label`).eq(2).should('be.visible').should(
            "contain.text",
            "Text Size and Color")

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__color`).eq(2).should('be.visible').should(
            "contain.value",
            "#")

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__range`).eq(2).should('be.visible')

        cy.getElement(`${invmedia_header}style-settings--picker .ml-form__input`).eq(2).should('be.visible').should(
            "contain.value",
            "px")
    });

    it(`should have Position from Left - Manual Alignment`, function () {
        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')

        cy.getElement(`${invmedia_header}style-settings--radio .ml-form__label`).eq(0).should('be.visible').should(
            "contain.text",
            "Position from Left")

        cy.getElement(`${invmedia_header}style-settings--radio .ml-form__label`).eq(1).should('be.visible').should(
            "contain.text",
            " Manual Alignment")

        cy.getElement(`.ml-icon-align-left`).should('be.visible')
        cy.getElement(`.ml-icon-align-center`).should('be.visible')
        cy.getElement(`.ml-icon-align-right`).should('be.visible')

    });

    it(`should have Position from Top - Manual Alignment`, function () {
        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')

        cy.getElement(`${invmedia_header}style-settings--radio .ml-form__label`).eq(2).should('be.visible').should(
            "contain.text",
            "Position from Top")

        cy.getElement(`${invmedia_header}style-settings--radio .ml-form__label`).eq(3).should('be.visible').should(
            "contain.text",
            " Manual Alignment")


        cy.getElement(`.vertical-icon`).its('length').should('eq', 3)
    });

    it(`should have Pencil and Dropdown to Replave/Reposition/Remove`, function () {

        cy.getElement(`.top-section__header--pencil  .ml-icon-pencil`).should('be.visible').click()
        cy.getElement(`.top-section__header--pencil__content`).first().should('be.visible')

        cy.getElement(`div[class="top-section__header--pencil__content ng-star-inserted"] a`).eq(0).should('be.visible').should(
            "contain.text",
            "Replace")

        cy.getElement(`div[class="top-section__header--pencil__content ng-star-inserted"] a`).eq(1).should('be.visible').should(
            "contain.text",
            "Reposition")

        cy.getElement(`div[class="top-section__header--pencil__content ng-star-inserted"] a`).eq(2).should('be.visible').should(
            "contain.text",
            "Remove")
        cy.getElement(`.top-section__header--pencil  .ml-icon-pencil`).click()
    });

    it(`should have Reposition Save/Cancel Messages`, function () {

        cy.getElement(`.top-section__header--pencil  .ml-icon-pencil`).click()

        cy.getElement(`div[class="top-section__header--pencil__content ng-star-inserted"] a`).eq(1).click()

        cy.getElement(`div[class="top-section__header--reposition-annotation ng-star-inserted"] p`).should('be.visible').should(
            "contain.text",
            "Move image for reposition")

        cy.getElement(`.top-section__header--button-groups .ml-button__red`).should('be.visible').should(
            "contain.text",
            "Save")

        cy.getElement(`.top-section__header--button-groups .ml-button__secondary`).should('be.visible').should(
            "contain.text",
            "Cancel").click()
    });

    it(`should have Image Remove confirmation Dialog`, function () {

        cy.getElement(`.top-section__header--pencil  .ml-icon-pencil`).click()

        cy.getElement(`div[class="top-section__header--pencil__content ng-star-inserted"] a`).eq(2).click()

        cy.getElement(`div[class="modal-body"] p`).should('be.visible').should(
            "contain.text",
            "Are you sure you want to remove this image")

        cy.getElement(`.modal-footer .btn-primary`).should('be.visible').should(
            "contain.text",
            "Yes")

        cy.getElement(`.modal-footer .btn-outline-danger`).should('be.visible').should(
            "contain.text",
            "No").click()
    });

    it(`should have "Go to Live View", "<-Pages" buttons`, function () {
        cy.getElement(`.investors-and-media-body .preview`).should('be.visible').should(
            "contain.text",
            "Go to Live View")

        cy.getElement(`.menu__footer .title`).should('be.visible').should(
            "contain.text",
            "GO BACK TO OLD DASHBOARD")

    });

    it(`should have "Save" button`, function () {

        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')
        cy.getElement('button[type="submit"]').first().should(
            "contain.text",
            "Save"
        )
    });

    it(`should have "Cancel" button`, function () {
        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')
        cy.getElement('button[type="button"]').should(
            "contain.text",
            "Cancel"
        )
    });
});

describe("Investiors and Media - Body", () => {

    // user details:
    let invmedia_header = ".Investors-and-media-header__"

    beforeEach(() => {
        cy.viewport(1440, 990)

    })

    before(() => {
        cy.login()
        cy.visit("/dashboard/2108/investors-and-media", {timeout: 20000})
        cy.getElement('.tabs-panel .ng-star-inserted').eq(1).click()
    })


    it(`should have Photo Gallery with edit icons`, function () {
        cy.get('.page-name').contains('Photo Gallery').should('be.visible').parent().within(() => {
            cy.get('.ml-icon-bin').should('be.visible')
            cy.get('.ml-icon-burger-menu').should('be.visible')
            cy.get('.ml-icon-next').should('be.visible')
            cy.get('.ml-icon-pencil').should('be.visible')

        });
    });

    it(`should have Documents with edit icons`, function () {
        cy.get('.page-name').contains('Documents').should('be.visible').parent().within(() => {
            cy.get('.ml-icon-pencil').should('be.visible')
            cy.get('.ml-icon-bin').should('be.visible')
            cy.get('.ml-icon-burger-menu').should('be.visible')
            cy.get('.ml-icon-next').should('be.visible')
        });
    });

    it(`should have Capital & Share Structure with edit icons`, function () {
        cy.get('.page-name').contains('Capital & Share Structure').should('be.visible').parent().within(() => {
            cy.get('.ml-icon-pencil').should('be.visible')
            cy.get('.ml-icon-bin').should('be.visible')
            cy.get('.ml-icon-burger-menu').should('be.visible')
            cy.get('.ml-icon-next').should('be.visible')
        });
    });


    it(`Add/Edit/ Remove new page working`, function () {
        cy.login()
        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')
        // textbox visible
        cy.get('.page-adding .page-input').should('be.visible').invoke('attr', 'placeholder').should(
            "contain",
            "Add a Page"
        )

        // adding new page
        cy.get('.page-adding .page-input').type("cypressPage")
        // plus button visible/clickable
        cy.get('.page-adding .ml-icon-add').should('be.visible').click().click()

        cy.getElement('.menu-scrollbar').first().scrollTo('bottom')
        cy.wait(2000)

        // new page visible/editable then removed
        cy.get('.page-name').contains('cypressPage').should('be.visible').parent().within(() => {
            cy.get('.ml-icon-bin').should('be.visible')
            cy.get('.ml-icon-burger-menu').should('be.visible')
            cy.get('.ml-icon-next').should('be.visible')
            cy.get('.ml-icon-pencil').should('be.visible').click()
        });
        cy.wait(1000)
        // check cypressPage nameedit mode
        cy.get('div[class="page-name-wrapp cdk-drag ng-star-inserted is-edit"]').should('be.visible')
        cy.get('.page-adding .page-input').click()

        cy.get('.page-name').contains('cypressPage').should('be.visible').parent().within(() => {
            cy.get('.ml-icon-bin').should('be.visible').click()
        });
        cy.wait(1000)
        cy.get('.page-name').contains('cypressPage').should('not.be.visible')
    });
});


describe("Investiors and Media - Navigation to Pages", () => {

    beforeEach(() => {
        cy.viewport(1440, 990)
        cy.login()
        cy.visit("/dashboard/2108/investors-and-media", {timeout: 20000})
        cy.getElement('.tabs-panel .ng-star-inserted').eq(1).click()
    })

    it(`should navigate to "Photo Gallery" page`, function () {
        cy.get('.page-name').contains('Photo Gallery').parent().within(() => {
            cy.get('.ml-icon-next').click()
        });
        cy.url().should('include', '/investors-and-media/photo-gallery')
    });

    it(`should navigate to "Documents" page`, function () {
        cy.get('.page-name').contains('Documents').parent().within(() => {
            cy.get('.ml-icon-next').click()
        });
        cy.url().should('include', '/investors-and-media/documents')
    });

    it(`should navigate to "Capital & Share Structure" page`, function () {
        cy.get('.page-name').contains('Capital & Share Structure').parent().within(() => {
            cy.get('.ml-icon-next').click()
        });
        cy.url().should('include', '/investors-and-media/capital-and-share-structure')
    });

});

